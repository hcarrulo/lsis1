#include <Arduino.h>
int PWMA= 19;
int INA1=5;
int INA2=23;
int PWMB=16;
int INB1=14;
int INB2=27;
int iman =18;
int caixa =25;
void setup() {
Serial.begin(9600);
ledcAttachPin(PWMA, 1);
pinMode(INA1, OUTPUT);
pinMode(INA2, OUTPUT);
ledcSetup(1,12000,8);
ledcAttachPin(PWMB, 2);
pinMode(INB1, OUTPUT);
pinMode(INB2, OUTPUT);
ledcSetup(2,12000,8);
pinMode(iman,OUTPUT);
pinMode(caixa,INPUT_PULLUP);
digitalWrite(iman,LOW);
}

void loop() {
motorA(100,false);
motorB(100,false);
if(digitalRead(caixa)==1){
    digitalWrite(iman,HIGH); 
    delay(5000);
 }
digitalWrite(iman,LOW);
}
void motorA(int pwm,boolean direcao){
  if(direcao==true){
  digitalWrite(INA1,LOW);
  digitalWrite(INA2,HIGH);
  ledcWrite(1,pwm);
  }else{
  digitalWrite(INA1,HIGH);
  digitalWrite(INA2,LOW);
  ledcWrite(1,pwm);
  }
}
void motorB(int pwm,boolean direcao){
  if(direcao==true){
  digitalWrite(INB1,LOW);
  digitalWrite(INB2,HIGH);
  ledcWrite(2,pwm);
  }else{
  digitalWrite(INB1,HIGH);
  digitalWrite(INB2,LOW);
  ledcWrite(2,pwm);
  }
}


//velocidade ledcWrite(1,127);
